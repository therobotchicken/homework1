#include "ofApp.h"
//--------------------------------------------------------------
void ofApp::setup(){
	angle = 0;
	angleChange = .01;
	rad = 0;
	radMax = 50;
	radDir = 1;
	strokeWeight = 1;
	hue = 0;
	hueDir = 1;
	pxPos, pyPos, xPos, yPos = 0;
	mode = "";

	ofBackground(0);
	ofSetBackgroundAuto(false);
	ofSetFrameRate(1200);
	ofEnableSmoothing();

	//initialize color in HSB
	c = ofColor(0);
	c.setHsb(hue, 99, 255);
	ofSetColor(c);

	ofSetLineWidth(strokeWeight);

	cout << "Radius: " << radMax << endl;
	cout << "Angle increment: " << angleChange << endl;
	cout << "Stroke width: " << strokeWeight << endl;

	/*gui.setup();
	gui.add(radSlider.setup("Radius", radMax, 1, 200));
	gui.add(strokeSlider.setup("Stroke Weight", strokeWeight, 1, 20));*/
}

//--------------------------------------------------------------
void ofApp::update(){
	//when mouse is released
	if (!ofGetMousePressed())
	{
		xPos = ofGetMouseX();
		pxPos = ofGetMouseX();
		yPos = ofGetMouseY();
		pyPos = ofGetMouseY();
		rad = 0;
		radDir = 1;
	}
	//when mouse clicked
	if (ofGetMousePressed() /*&& !(ofGetMouseX() > 0 && ofGetMouseX() < 220 && ofGetMouseY() > 0 && ofGetMouseY() < 75)*/)
	{
		//increment angle
		angle += angleChange;
		//increment or decrement radius
		//-bounces back and forth between 0 and chosen max radius
		rad += radDir;
		if (rad == 0 || rad >= radMax)
			radDir *= -1;
		//change color
		//-bounces back and forth between hue values of 0 and 359
		hue += hueDir;
		if (hue == 0 || hue == 359)
			hueDir *= -1;
		c.setHue(hue);
		ofSetColor(c);
		//current x and y values become previous x and previous y
		//current x and y get new positions
		pxPos = xPos;
		pyPos = yPos;
		xPos = ofGetMouseX() + cos(angle) * rad;
		yPos = ofGetMouseY() + sin(angle) * rad;
	}

	/*radMax = radSlider;
	ofSetLineWidth(strokeSlider);*/
}

//--------------------------------------------------------------
void ofApp::draw(){
	if (ofGetMousePressed())
	{
		//The spirographs are actually a collection of very short lines.
		ofDrawLine(pxPos, pyPos, xPos, yPos);
	}

	//gui.draw();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	//radius mode
	if (key == 'r' || key == 'R')
	{
		mode = "radius";
		cout << "mode: radius" << endl;
	}

	//angle mode
	if (key == 'a' || key == 'A')
	{
		mode = "angle";
		cout << "mode: angle" << endl;
	}

	//stroke mode
	if (key == 's' || key == 'S')
	{
		mode = "stroke";
		cout << "mode: stroke" << endl;
	}

	//reset canvas
	if (key == 'x' || key == 'X')
	{
		ofBackground(0);
		cout << "canvas erased" << endl;
	}

	//up and down keys change settings of current mode
	//-radius in increments of 10
	//-angle in increments of .002
	//-stroke in increments of 1
	if (key == OF_KEY_UP)
	{
		if (mode == "")
			cout << "No mode selected. Press A for angle, R for radius, or S for stroke." << endl;
		else if (mode == "radius")
		{
			radMax += 10;
			cout << "radius = " << radMax << endl;
		}
		else if (mode == "angle")
		{
			angleChange += .002;
			cout << "angle increment (radians) = " << angleChange << endl;
		}
		else if (mode == "stroke")
		{
			strokeWeight += 1;
			ofSetLineWidth(strokeWeight);
			cout << "stroke weight = " << strokeWeight << endl;
		}
	}
	else if (key == OF_KEY_DOWN)
	{
		if (mode == "")
			cout << "No mode selected. Press A for angle, R for radius, or S for stroke." << endl;
		else if (mode == "radius")
		{
			radMax -= 10;
			cout << "radius = " << radMax << endl;
		}
		else if (mode == "angle")
		{
			angleChange -= .002;
			cout << "angle increment (radians) = " << angleChange << endl;
		}
		else if (mode == "stroke")
		{
			strokeWeight -= 1;
			ofSetLineWidth(strokeWeight);
			cout << "stroke weight = " << strokeWeight << endl;
		}
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
