#pragma once

#include "ofMain.h"
#include "ofxGui.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
		//global variables
		float angle;
		float angleChange;
		float rad;
		float radMax;
		int radDir;
		int strokeWeight;
		int hue;
		int hueDir;
		float pxPos, pyPos, xPos, yPos;
		string mode;
		ofColor c;
		ofxFloatSlider radSlider;
		ofxIntSlider strokeSlider;
		ofxPanel gui;
};
